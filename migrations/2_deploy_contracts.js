const Token = artifacts.require("Token")

module.exports = async function (deployer) {

    const name = "Cebula Investment"
    const symbol = "CEBULA"
    const supply = web3.utils.toWei('10000', 'ether') // 10000 Tokens

    await deployer.deploy(Token, name, symbol, supply)
};